"""
DtwMzmlAligner.py
Original Author: Jarrett Egertson
Author Date: 8/22/2016

Aligns mzml files to each other using a dynamic time warp of the MS1 signal.
"""

import pymzml
import numpy as np
import scipy.spatial.distance
import matplotlib.pyplot as plt
from SpectrumPeakExtractor import SpectrumPeakExtractorFactory
from scipy import interpolate
from itertools import groupby
from dtwjs import DtWarper
import seaborn as sns
from scipy.signal import savgol_filter
import os

PRTC_MZS = [493.7683,
            496.2867,
            498.8018,
            558.326,
            573.3025,
            586.8003,
            613.3168,
            680.3736,
            695.8324,
            745.3925,
            773.8956,
            787.4212,
            801.4115]


def sq_cos(x, y):
    return scipy.spatial.distance.cosine(x, y) ** 2


def bin_spectrum(spec):
    bins = np.arange(400, 2000, 0.1)
    return np.array(np.histogram(spec.mz, bins=bins, weights=spec.i)[0])


def dist_cos(x, y):
    if sum(x) + sum(y) == 0.0:
        return 0.5
    else:
        return scipy.spatial.distance.cosine(x, y)


def dist_cos_squared(x, y):
    return dist_cos(x, y) ** 2


class DtwMzmlAligner(object):
    def __init__(self, mzml_ref, svg_smooth=False):
        self._mzml_ref = mzml_ref
        self._svg_smooth = svg_smooth

        # store most recent query
        self._most_recent_query = None
        # mz values of spiked in calibrants used to visualize alignment
        self._calibrant_mzs = PRTC_MZS

        # peak extractor to extract calibrant signals from spectra
        self._calibrant_extractor = SpectrumPeakExtractorFactory.get_ppm_extractor(self._calibrant_mzs)

        # transformed spectra used for DTW alignment of query to ref
        self._ref_train = None
        self._query_train = None

        # extracted calibrant signal for ref and query
        # each row is a calibrant, columns are RT
        self._ref_calibrant_xic = None
        self._query_calibrant_xic = None

        # RT values for each scan
        self._ref_rt = None
        self._query_rt = None

        # best path from alignment
        self._path = None
        # bijective path
        self._bijective_path = None
        # function to convert query RT to reference RT
        self._get_aligned_rt = None

        # distance matrix
        self._distances = None

        # intialize the reference
        self._ref_rt, self._ref_train, ref_calibrant_xic = self.extract_training_data(mzml_ref)
        self._ref_calibrant_xic = np.array(ref_calibrant_xic).T

    def extract_training_data(self, mzml_file):
        rts = list()
        train_set = list()
        calibrant_set = list()
        for s in pymzml.run.Reader(mzml_file):
            if s['ms level'] != 1:
                continue
            train_set.append(bin_spectrum(s))
            calibrant_set.append(self._calibrant_extractor.get_binned(s))
            rts.append(s['scan start time'])
        train_set = np.array(train_set)
        if self._svg_smooth:
            train_set = savgol_filter(train_set, 11, 2, axis=0)

        return rts, train_set, calibrant_set

    def train(self, mzml_query):
        query_rt, query_train, query_calibrant_xic = self.extract_training_data(mzml_query)
        # distance, _, _, path = dtw(ref_train, query_train, dist=scipy.spatial.distance.cityblock)
        warper = DtWarper(transition_weights=(1.0, 1.0, 1.0 / 2.1), origin_start=True, origin_end=True)
        path, self._distances = warper.do_warp(self._ref_train, query_train)
        # path = zip(*path)
        self._query_train = query_train
        self._query_rt = query_rt
        # keep one y value for every x in the path
        bijective_path = self.make_path_bijective(path)
        self._path = path
        self._bijective_path = bijective_path
        path_x, path_y = zip(*self._bijective_path)
        path_rt_x = [self._ref_rt[ind] for ind in path_x]
        path_rt_y = [self._query_rt[ind] for ind in path_y]
        path_rt_x.insert(0, 0)
        path_rt_y.insert(0, 0)
        max_rt = max((self._ref_rt[-1], self._query_rt[-1]))
        path_rt_x.append(max_rt + 1)
        path_rt_y.append(max_rt + 1)
        self._get_aligned_rt = interpolate.interp1d(path_rt_y, path_rt_x, kind='linear')
        # tck = interpolate.splprep([path_rt_y, path_rt_x], s=0.0)
        # self._get_aligned_rt = interpolate.splev()

        self._query_calibrant_xic = np.array(query_calibrant_xic).T
        self._most_recent_query = mzml_query
        return interpolate.interp1d(path_rt_y, path_rt_x, kind='linear')

    @staticmethod
    def make_path_bijective(path):
        bijective_path_first_pass = list()
        bijective_path_second_pass = list()
        for k, g in groupby(path, lambda x: x[0]):
            group_vals = list(g)
            # if there are multiple entries for a reference RT, add the median entry
            bijective_path_first_pass.append((k, group_vals[len(group_vals) // 2][1]))
        for k, g in groupby(bijective_path_first_pass, lambda x: x[1]):
            group_vals = list(g)
            bijective_path_second_pass.append((group_vals[len(group_vals) // 2][0], k))
        return bijective_path_second_pass

    def plot_alignment(self, fig_plot=None):
        """
        Plot the results of the most recent alignment call
        :param fig_plot: a matplotlib figure object to draw the figure on. If None -- interactive figure will be
                displayed.
        :return:
        """
        sns.set_style("white")
        sns.set_context("talk")
        sns.set_palette(sns.hls_palette(len(self._calibrant_mzs), l=.3, s=.8))
        num_calibrants = self._ref_calibrant_xic.shape[0]
        if fig_plot:
            plt.figure(fig_plot.number)
        else:
            fig = plt.figure()

        ax_path = plt.subplot2grid((5, 1), (0, 0))
        ax_path.set_title("Query: %s" % os.path.basename(self._most_recent_query) + '\n' +
                          "Reference: %s" % os.path.basename(self._mzml_ref), fontsize=12.0)
        path_x, path_y = zip(*self._path)
        path_rt_y = [self._query_rt[ind] for ind in path_y]
        ax_path.axhline(ls='--', color='gray')

        # plot smoothed path
        y_smoothed = np.arange(path_rt_y[0], path_rt_y[-1], 1.0 / 60.0)
        x_smoothed = self._get_aligned_rt(y_smoothed)
        ax_path.plot(x_smoothed, 60.0 * (x_smoothed - y_smoothed), label='smoothed bijective', color='black')
        ax_path.set_ylabel("RT Correction\n(seconds)", fontsize=12)

        ax_unaligned = plt.subplot2grid((5, 1), (1, 0), rowspan=2, sharex=ax_path)
        ax_aligned = plt.subplot2grid((5, 1), (3, 0), rowspan=2, sharex=ax_unaligned)
        aligned_query_rts = self._get_aligned_rt(self._query_rt)
        for i in range(num_calibrants):
            xic_ref = -1.0 * self._ref_calibrant_xic[i]
            xic_query = self._query_calibrant_xic[i]
            c_line = ax_unaligned.plot(self._ref_rt, xic_ref, lw=2)[0]
            col = plt.getp(c_line, 'color')
            ax_unaligned.plot(self._query_rt, xic_query, color=col, lw=2)
            ax_aligned.plot(self._ref_rt, xic_ref, color=col, lw=2)
            ax_aligned.plot(aligned_query_rts, xic_query, color=col, lw=2)
        ax_unaligned.axhline(ls='-', color='black', lw=2)
        ax_aligned.axhline(ls='-', color='black', lw=2)
        ax_aligned.set_xlabel('Retention time (minutes)', fontsize=14)
        ax_aligned.set_ylabel("Intensity")
        ax_unaligned.set_ylabel("Intensity")
        for ax_to_label in (ax_unaligned, ax_aligned):
            ax_to_label.text(0, 0.0, "Reference",
                             horizontalalignment="left",
                             verticalalignment="top",
                             transform=ax_to_label.get_yaxis_transform(),
                             fontsize=12)
            ax_to_label.text(0, 0.0, "Query",
                             horizontalalignment="left",
                             verticalalignment="bottom",
                             transform=ax_to_label.get_yaxis_transform(),
                             fontsize=12)
        plt.tight_layout(h_pad=0, w_pad=0)
        if not fig_plot:
            plt.show()
            plt.close()

            # fig = plt.figure(figsize=(12, 12), dpi=150)
            # ax = sns.heatmap(self._distances)
            # plt.savefig("C:\\Users\\jegertso\\Documents\\work\\dtw_dist\\dist.png")
            # plt.close()

            # fig = plt.figure(figsize=(12, 12), dpi=150)
            # ax = sns.heatmap(np.log10(self._distances[1230:1390, 1200:1370]))
            # plt.savefig("C:\\Users\\jegertso\\Documents\\work\\dtw_dist\\dist_zoom.png")
            # plt.close()
