"""
SpectrumPeakExtractor.py
Original Author: Jarrett Egertson
Author Date: 8/22/2016

Extracts peaks from a spectrum (pymzml) using mass error in daltons or ppm
"""

from bisect import bisect_left


class SpectrumPeakExtractorFactory(object):
    def __init__(self):
        pass

    @staticmethod
    def get_ppm_extractor(mz_targets, mass_error_ppm=10.0):
        """
        :param mz_targets: a list of m/z values to extract
        :param mass_error_ppm: mass error in parts per million
        :return: a SpectrumPeakExtractor for the targets
        """
        intervals = [SpectrumPeakExtractorFactory.ppm_interval(mz, mass_error_ppm) for mz in sorted(mz_targets)]
        return SpectrumPeakExtractor(intervals)

    @staticmethod
    def ppm_interval(mz, mass_error_ppm):
        delta_mz = mass_error_ppm * mz / 1.0e6
        return mz - delta_mz, mz + delta_mz


class SpectrumPeakExtractor(object):
    def __init__(self, intervals):
        # sort the intervals by interval start
        self._intervals = sorted(intervals, key=lambda x: x[0])
        self._max_delta = max([entry[1] - entry[0] for entry in intervals])
        self._min_value = self._intervals[0][0]
        self._max_value = self._intervals[-1][0] + self._max_delta

    def get_binned(self, spectrum):
        """ spectrum must be sorted mzs"""
        # initialize the filtered values to be 0
        binned_intensities = [0.0] * len(self._intervals)
        mz_array = spectrum.mz
        intensity_array = spectrum.i
        bin_start_index = 0
        query_start_index = bisect_left(mz_array, self._min_value)
        for query_index in range(query_start_index, len(mz_array)):
            # iterate through each "query" peak in the MS/MS spectrum to be filtered
            query = mz_array[query_index]
            if query < self._min_value:
                continue
            if query > self._max_value:
                break
            min_start = query - self._max_delta
            # move the starting point for this search and future searches to the first bin
            # that could possibly contain this peak
            while bin_start_index < len(self._intervals):
                if self._intervals[bin_start_index][0] >= min_start:
                    break
                bin_start_index += 1
            # look forward for peak bins that contain this query
            for bin_index in range(bin_start_index, len(self._intervals)):
                if self._intervals[bin_index][0] > query:
                    break
                if self._intervals[bin_index][0] <= query < self._intervals[bin_index][1]:
                    binned_intensities[bin_index] += intensity_array[query_index]
        return binned_intensities




