"""
DtwGpfAlign.py
Original Author: Jarrett Egertson
Author Date: 8/15/2016

A script for aligning retention times in an Encyclopedia library (elib).
It is designed specifically for libraries generated using gas phase fractionation DIA with a wide window MS1 scan
even included for the purpose of retention time alignment. It uses a dynamic time warp alignment of these
MS1 scan events to generate an alignment to a seed file.
"""

import argparse
import os
import shutil
from sqlalchemy import create_engine, MetaData
import pandas as pd
from DtwMzmlAligner import DtwMzmlAligner
import time
import logging
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import sqlalchemy.sql.sqltypes as sqtypes


class ElibRetentionAligner(object):
    def __init__(self, elib_file, mzml_dir, plot_output=None):
        self._engine = create_engine("sqlite:///%s" % elib_file)
        # all required information is in the entries data tables in encyclopedia
        self._entries_df, self._entries_dtypes = self.read_entries_table()
        self._mzmlDir = mzml_dir
        self._reference = None
        self._plot_output = plot_output
        self._alignments = dict()
        self.get_alignments()

    @staticmethod
    def get_dtypes(engine, tablename):
        meta = MetaData(engine, True)
        table = meta.tables[tablename]
        dtypes = dict()
        for col in table.columns:
            dtypes[col.name] = col.type
        dtypes['PeptideSeq'] = sqtypes.String
        dtypes['PeptideModSeq'] = sqtypes.String
        dtypes['SourceFile'] = sqtypes.String
        return dtypes

    def read_entries_table(self):
        entries_df = pd.read_sql_table('entries', self._engine)
        dtypes = self.get_dtypes(self._engine, 'entries')
        return entries_df, dtypes

    def get_alignments(self):
        mzml_names = [os.path.join(self._mzmlDir, entry) for entry in self._entries_df['SourceFile'].unique()]
        for fname in mzml_names:
            if not os.path.exists(fname):
                raise Exception("Could not find required mzml file %s." % fname)
        self._reference = mzml_names[0]
        d = DtwMzmlAligner(self._reference, svg_smooth=True)
        if self._plot_output:
            pdf = PdfPages(self._plot_output)
        for fname in mzml_names[1:]:
            t0 = time.clock()
            alignment = d.train(fname)
            t1 = time.clock()
            logging.info("%d seconds to train" % (t1 - t0))
            if self._plot_output:
                d.plot_alignment(fig_plot=plt.figure(figsize=(10, 7.5)))
                pdf.savefig()
                plt.close()
            self._alignments[os.path.basename(fname)] = alignment
        if self._plot_output:
            pdf.close()

    def rt_lambda(self, x):
        y = x.copy()
        if x['SourceFile'] in self._alignments:
            original_rt_minutes = x['RTInSeconds'] / 60.0
            new_rt_seconds = self._alignments[x['SourceFile']](original_rt_minutes) * 60.0
            rt_shift = new_rt_seconds - x['RTInSeconds']
            y['RTInSeconds'] = new_rt_seconds
            y['RTInSecondsStart'] += rt_shift
            y['RTInSecondsStop'] += rt_shift
        return y

    def write_aligned_rts(self):
        aligned_entries = self._entries_df.apply(self.rt_lambda, axis=1)
        aligned_entries.to_sql('entries',
                               self._engine,
                               if_exists='replace',
                               dtype=self._entries_dtypes,
                               index=False)


def init_argparse():
    parser = argparse.ArgumentParser(
        description="DtwGpfAlign aligns retention time internally within an elib generated from gas phase fractionation"
                    "DIA data.  It takes as input an elib file, as well as mzML files containing only the alignment"
                    "MS1 scan events.  The alignment MS1 scan is a wide window MS1 scan that should cover more of the"
                    "m/z range than the MS/MS scans in the individual gas phrase fraction",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--prtc', default=False, action='store_true',
                        help="If PRTC peptides were included, extracted ion chromatograms will be used for alignment")
    parser.add_argument('--mzmlDir', default=os.getcwd(), type=os.path.abspath,
                        help="Directory to search for mzml files")
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output elib library if it already exists")
    parser.add_argument('--drawPlots', type=os.path.abspath, help="Specify an output pdf file to draw alignments in.")
    parser.add_argument("inputElibFile", type=os.path.abspath, help="Input elib file")
    parser.add_argument("outputElibFile", type=os.path.abspath, help="Output elib file")
    return parser

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputElibFile) and not args.overwrite:
        raise Exception("Output elib file %s already exists! Aborting." % args.outputElibFile)
    shutil.copy(args.inputElibFile, args.outputElibFile)
    elib_aligner = ElibRetentionAligner(args.outputElibFile, args.mzmlDir, plot_output=args.drawPlots)
    elib_aligner.write_aligned_rts()
    logging.info("Done!")

