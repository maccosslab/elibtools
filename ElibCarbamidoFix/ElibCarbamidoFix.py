"""
ElibCarbamidoFix.py
Original Author: Jarrett Egertson
Author Date: 9/12/2016

This is a very specific script designed to fix .elib files where the carbamidomethylated cysteine was not correctly s
specified in the peptide mod seq line
"""

import argparse
import os
import pandas as pd
from sqlalchemy import create_engine, MetaData
import shutil
import sqlalchemy.sql.sqltypes as sqtypes
import logging


def get_dtypes(tablename, engine):
    meta = MetaData(engine, True)
    table = meta.tables[tablename]
    dtypes = dict()
    for col in table.columns:
        dtypes[col.name] = col.type
    dtypes['PeptideSeq'] = sqtypes.String
    dtypes['PeptideModSeq'] = sqtypes.String
    dtypes['SourceFile'] = sqtypes.String
    return dtypes


def init_argparse():
    parser = argparse.ArgumentParser(
        description="ElibCabamidoFix corrects an error that happened in elib files written out from a Pecan "
                    "search in Encyclopedia v 0.2.9 or earlier. These files did not write out the +57.0 "
                    "on cysteine in the PeptideModSeq entry of the entries table in the elib file. This "
                    "script adds the +57 annotation to all cysteines missing the +57 annotation",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--overwrite', default=False, action='store_true',
                        help="Overwrite the output elib file if it already exists")
    parser.add_argument("inputElibFile", type=os.path.abspath, help="Input elib file")
    parser.add_argument("outputElibFile", type=os.path.abspath, help="Output elib file")
    return parser


def fix_carb_string(s):
    return s.replace('C', 'C[+57.021465]')

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    parser = init_argparse()
    args = parser.parse_args()
    if os.path.exists(args.outputElibFile) and not args.overwrite:
        raise Exception("Output elib file %s already exists! Aborting." % args.outputElibFile)
    shutil.copy(args.inputElibFile, args.outputElibFile)
    engine = create_engine("sqlite:///%s" % args.outputElibFile)
    entries_df = pd.read_sql_table('entries', engine)
    entries_dtypes = get_dtypes('entries', engine)
    entries_df.loc[:, 'PeptideModSeq'] = entries_df['PeptideModSeq'].apply(fix_carb_string)
    entries_df.to_sql('entries', engine, if_exists='replace', dtype=entries_dtypes, index=False)
    logging.info("DONE. WOW BRIAN, JUST WOW")

